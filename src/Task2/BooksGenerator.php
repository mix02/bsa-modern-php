<?php

declare(strict_types=1);

namespace App\Task2;

class BooksGenerator
{
    public function __construct(
        public int $minPagesNumber, 
        public array $libraryBooks,
        public int $maxPrice,
        public array $storeBooks)
    {
        
    }

    public function generate(): \Generator
    {

        foreach ($this->libraryBooks as $libBook) {

            if ($libBook->getPagesNumber() >= $this->minPagesNumber)  {
                yield $libBook;
            }
        }

        foreach ($this->storeBooks as $storeBook) {

            if ($storeBook->getPrice() <= $this->maxPrice)  {
                yield $storeBook;
            }
        }
    }
}