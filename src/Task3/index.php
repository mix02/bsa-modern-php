<?php

require __DIR__ . '/../../vendor/autoload.php';

use App\Task1\Track;
use App\Task3\CarTrackHtmlPresenter;

$track = new Track(4, 40);

$presenter = new CarTrackHtmlPresenter();
$presentation = $presenter->present($track);


?>

<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Built-in Web Server</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
</head>
<body>
    <div class="container">
        <div class="row">
            <div class="col-12">
                <table class="table table-image">
                    <tr>
                        <th>ID</th>
                        <th>Image</th>
                        <th>Name</th>
                        <th>Speed</th>
                        <th>Pit Stop Time</th>
                        <th>Fuel Consumption</th>
                        <th>Fuel Tank Volume</th>
                    </tr>
                    <?php foreach ($presentation as $present): ?>
                        <tr>
                            <td><?php echo $present['id']; ?></td>
                            <td><img src="<?php echo $present['image']; ?>"></td>
                            <td><?php echo $present['name']; ?></td>
                            <td><?php echo $present['speed']; ?></td>  
                            <td><?php echo $present['pitStopTime']; ?></td>  
                            <td><?php echo $present['fuelConsumption']; ?></td>  
                            <td><?php echo $present['fuelTankVolume']; ?></td>  
                        </tr>
                    <?php endforeach; ?>
                </table>
            </div>
        </div>
    </div>

</body>
</html>
