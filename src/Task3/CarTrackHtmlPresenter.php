<?php

declare (strict_types = 1);

namespace App\Task3;

use App\Task1\Car;
use App\Task1\Track;

class CarTrackHtmlPresenter
{
    public function present(Track $track): array
    {
        $car1 = new Car(
            1,
            'https://pbs.twimg.com/profile_images/595409436585361408/aFJGRaO6_400x400.jpg',
            'BMW',
            250,
            10,
            5,
            15
        );
        $car2 = new Car(
            2,
            'https://i.pinimg.com/originals/e4/15/83/e41583f55444b931f4ba2f0f8bce1970.jpg',
            'Tesla',
            200,
            5,
            5.3,
            18
        );
        $car3 = new Car(
            3,
            'https://fordsalomao.com.br/wp-content/uploads/2019/02/1499441577430-1-1024x542-256x256.jpg',
            'Ford',
            220,
            5,
            6.1,
            18.5
        );

        $track->add($car1);
        $track->add($car2);
        $track->add($car3);

        $cars = [];
        
        foreach ($track->all() as $car) {

            $cars[] = [
                'id' => $car->getId(),
                'image' => $car->getImage(),
                'name' => $car->getName(),
                'speed' => $car->getSpeed(),
                'pitStopTime' => $car->getPitStopTime(),
                'fuelConsumption' => $car->getFuelConsumption(),
                'fuelTankVolume' => $car->getFuelTankVolume(),
            ];
        }

    
        return $cars;
    }
}
