<?php

declare(strict_types=1);

namespace App\Task1;

class Track
{

    private const KILOMETR_PER_LITER = 100;

    private float $lapLength;
    private int $lapsNumber;
    private array $cars; 
    public function __construct(float $lapLength, int $lapsNumber)
    {
        $this->lapLength = $lapLength;
        $this->lapsNumber = $lapsNumber;
    }

    public function getLapLength(): float
    {
        return $this->lapLength;
    }

    public function getLapsNumber(): int
    {
        return $this->lapsNumber;
    }

    public function add(Car $car): void
    {
        $this->cars[] = $car;
    }

    public function all(): array
    {
        return $this->cars;
    }

    public function run()
    {
        $distance = $this->lapLength * $this->lapsNumber;

        $timesWithCars = array_map(function ($car) use ($distance) {

            $cleanTime = $distance / $car->getSpeed() * 3600;
            $driveOnAFullTank = $car->getFuelTankVolume() * self::KILOMETR_PER_LITER / $car->getFuelConsumption();
            $numberPitStops = $distance >= $driveOnAFullTank ?  floor($distance / $driveOnAFullTank) : 0;
            $howLongPitStop = $car->getPitStopTime() * $numberPitStops;
            $totalTime = $cleanTime + $howLongPitStop;

            return [$totalTime => $car];

        }, $this->cars);

        $minTime = PHP_FLOAT_MAX; 
        foreach ($timesWithCars as $time) {
            if ($time < $minTime) {
                $minTime = $time;
            }
        }

        return $this->cars[$minTime];

    }
}